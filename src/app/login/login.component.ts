import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  constructor(private fb : FormBuilder , private router : Router) { 
    localStorage.removeItem('id');
  }
  loginForm : FormGroup;
  username: string;
  password: string;
  id : number;
  usermatch : boolean;
    ngOnInit() {
      localStorage.removeItem('id');
      this.loginForm = this.fb.group({
        username: [''],
        password: ['']
    })
    };

    public hasError = (controlName: string, errorName: string) =>{
      return this.loginForm.controls[controlName].hasError(errorName);
    }

    login() : void {

console.log('login clicked');
this.router.navigate(["home"]);
      
    }

    register() : void {
      let randomid = Math.floor(100000 + Math.random() * 900000);
      console.log(randomid);
      localStorage.setItem('username',JSON.stringify(randomid));
      this.router.navigate(["register"]);
    }
    

}
